package com.faysal.mist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.automobile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Clicked on the automobile button", //Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),InfoActivity.class));
            }
        });
        findViewById(R.id.electrical).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Clicked on the electrical button", //Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),InfoActivity.class));
            }
        });
        findViewById(R.id.civil).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Clicked on the civil button", //Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),InfoActivity.class));
            }
        });
        findViewById(R.id.architecture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Clicked on the architecture button", //Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),InfoActivity.class));
            }
        });
        findViewById(R.id.computer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Clicked on the computer button", //Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),InfoActivity.class));
            }
        });
        findViewById(R.id.mecanical).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Clicked on the mechanical button", //Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),InfoActivity.class));
            }
        });



    }
}
